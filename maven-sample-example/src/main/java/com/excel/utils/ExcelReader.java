package com.excel.utils;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;



public class ExcelReader {
	public static  String SAMPLE_XLSX_FILE_PATH;
	
	public ExcelReader() {
	
	}
	
	
	public void readFile(String fileName)  {
		SAMPLE_XLSX_FILE_PATH = fileName; //"./new-file.xlsx";
		try {
		Workbook workbook = WorkbookFactory.create(new File(SAMPLE_XLSX_FILE_PATH));
		System.out.println("Workbook has " + workbook.getNumberOfSheets() + " Sheets : ");
		

        Iterator<Sheet> sheetIterator = workbook.sheetIterator();
        System.out.println("Retrieving Sheets using Iterator");
        while (sheetIterator.hasNext()) {
            Sheet sheet = sheetIterator.next();
            System.out.println("=> " + sheet.getSheetName());
        }

      

        
        Sheet sheet = workbook.getSheetAt(0);

      
        DataFormatter dataFormatter = new DataFormatter();

       
        System.out.println("\n\nIterating over Rows and Columns using for-each loop\n");
        for (Row row: sheet) {
            for(Cell cell: row) {
                String cellValue = dataFormatter.formatCellValue(cell);
                System.out.print(cellValue + "\t");
            }
            System.out.println();
        }

    
        workbook.close();
	}
	catch(IOException e) {
		e.printStackTrace();
	}
	catch(InvalidFormatException e) {
		e.printStackTrace();
	}

	}
	
   
}
