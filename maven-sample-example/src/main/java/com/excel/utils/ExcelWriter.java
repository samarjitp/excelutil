package com.excel.utils;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.clientapp.Employee;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;


public class ExcelWriter {

    private static String[] columns = {"Name", "Email", "Date Of Birth", "Salary"};

   

    public  void writeFile(List<Employee> employees , String fileName) {

     try {
        Workbook workbook = new XSSFWorkbook();   
        CreationHelper createHelper = workbook.getCreationHelper();

        Sheet sheet = workbook.createSheet("EmployeePersonalInfo");
        
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.RED.getIndex());

        
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

       
        Row headerRow = sheet.createRow(0);

     
        for(int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

       
        CellStyle dateCellStyle = workbook.createCellStyle();
        dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));

       
        int rowNum = 1;
        for(Employee employee: employees) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(employee.getName());
            row.createCell(1).setCellValue(employee.getEmail());

            Cell dateOfBirthCell = row.createCell(2);
            dateOfBirthCell.setCellValue(employee.getDateOfBirth());
            dateOfBirthCell.setCellStyle(dateCellStyle);

            row.createCell(3)
                    .setCellValue(employee.getSalary());
        }

        
        for(int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        FileOutputStream fileOut = new FileOutputStream(fileName);
        workbook.write(fileOut);
        fileOut.close();

        workbook.close();
        System.out.print("Completed");
    }
    catch(IOException e) {
 		e.printStackTrace();
 	}

}



    private  void modifyFile(String fileName , int rownum , int cellnum , String value) {
	    	try {
		        Workbook workbook = WorkbookFactory.create(new File(fileName));
		        Sheet sheet = workbook.getSheetAt(0);
		
		      
		        Row row = sheet.getRow(rownum);
		        Cell cell = row.getCell(cellnum);
		        
		        if (cell == null)
		            cell = row.createCell(cellnum);
		
		        cell.setCellType(CellType.STRING);
		        cell.setCellValue(value);
		
		      
		        FileOutputStream fileOut = new FileOutputStream(fileName);
		        workbook.write(fileOut);
		        fileOut.close();
		        workbook.close();
		        
		    }
	    	catch(IOException e) {
		    	e.printStackTrace();
		    }catch(InvalidFormatException e) {
		    	e.printStackTrace();
		    }
    }
}


