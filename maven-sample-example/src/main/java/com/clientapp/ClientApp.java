package com.clientapp;



import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.excel.utils.ExcelReader;
import com.excel.utils.ExcelWriter;

public class ClientApp {

	public static void createExcel(String fileName) {
		List<Employee> employees =  new ArrayList<>();
		Calendar dateOfBirth = Calendar.getInstance();
        dateOfBirth.set(1999, 01, 01);
        employees.add(new Employee("Samarjit Pal", "samarjit@example.com",
                dateOfBirth.getTime(), 10000000.0));

        dateOfBirth.set(1965, 10, 15);
        employees.add(new Employee("Vikas Chaurasiya", "vikas@example.com",
                dateOfBirth.getTime(), 1100000.0));

        dateOfBirth.set(1987, 4, 18);
        employees.add(new Employee("Neil Dutta", "neilexample.com",
                dateOfBirth.getTime(), 600000.0));
        
        ExcelWriter excelwriter = new ExcelWriter();
        excelwriter.writeFile(employees,fileName);
        
	}
	public static void main(String args[]) {
		String fileName = "./new-file.xlsx";
		createExcel(fileName);
		
		ExcelReader excelreader = new ExcelReader();
		excelreader.readFile(fileName);
		
	}
}
